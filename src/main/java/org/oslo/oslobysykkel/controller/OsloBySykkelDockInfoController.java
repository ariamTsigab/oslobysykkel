package org.oslo.oslobysykkel.controller;


import lombok.extern.slf4j.Slf4j;
import org.oslo.oslobysykkel.consumer.OsloBysykkelConsumer;
import org.oslo.oslobysykkel.domain.OsloBySykkelDockInfo;
import org.oslo.oslobysykkel.domain.map.OsloBySykkelDockInfoMapper;
import org.oslo.oslobysykkel.exception.OsloBysykkelFunctionalException;
import org.oslo.oslobysykkel.exception.OsloBysykkelTechnicalException;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import javax.inject.Inject;
import java.util.List;

/**
 * @author Tsigab Angosom Gebremedhin
 */

@Slf4j
@RestController
@RequestMapping("/rest/api")
public class OsloBySykkelDockInfoController {

    private final OsloBysykkelConsumer osloBysykkelConsumer;
    private final OsloBySykkelDockInfoMapper mapper;

    @Inject
    public OsloBySykkelDockInfoController(OsloBysykkelConsumer osloBysykkelConsumer) {
        this.osloBysykkelConsumer = osloBysykkelConsumer;
        this.mapper = new OsloBySykkelDockInfoMapper();
    }

    @ResponseBody
    @GetMapping(value = "/{orgName}/stations")
    public ResponseEntity<List<OsloBySykkelDockInfo>> getOsloBySykkelogDockStatusInfo(@PathVariable String orgName) {

        List<OsloBySykkelDockInfo> staionsInfo = mapper.map(osloBysykkelConsumer.getSykkelStationInfo(orgName),
                osloBysykkelConsumer.getSykkelStationStatus(orgName));

        try {
            if(staionsInfo==null || staionsInfo.isEmpty()) {
                return ResponseEntity.notFound().build();
            }
            return ResponseEntity.ok().body(staionsInfo);
        } catch (OsloBysykkelFunctionalException e) {
            log.warn("getOsloBikesStatus- feilet funksjonelt ved søk på Oslo bysykkel statatsjonInfo. Feilmelding={}", e.getMessage());
            throw e;
        } catch (OsloBysykkelTechnicalException e) {
            log.error("getOsloBikesStatus- feilet teknisk ved søk på sslo bysykkel statatsjonInfo. Feilmelding={}", e.getMessage());
            throw e;
        }

    }

}
