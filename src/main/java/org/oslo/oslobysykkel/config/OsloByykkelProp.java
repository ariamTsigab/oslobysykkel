package org.oslo.oslobysykkel.config;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.lang.NonNull;
import org.springframework.validation.annotation.Validated;

/**
 * @author Tsigab Angosom Gebremedhin
 */

@Getter
@Setter
@ToString
@Validated
@ConfigurationProperties("oslobysykkel")
public class OsloByykkelProp {

    @NonNull
    private String url;

    @NonNull
    private String appName;
}
