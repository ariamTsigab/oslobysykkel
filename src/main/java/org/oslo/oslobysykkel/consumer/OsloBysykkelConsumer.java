package org.oslo.oslobysykkel.consumer;


import lombok.extern.slf4j.Slf4j;
import org.oslo.oslobysykkel.config.OsloByykkelProp;
import org.oslo.oslobysykkel.domain.StationInfoResponse;
import org.oslo.oslobysykkel.domain.StationStatusResponse;
import org.oslo.oslobysykkel.exception.OsloBysykkelFunctionalException;
import org.oslo.oslobysykkel.exception.OsloBysykkelTechnicalException;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.retry.annotation.Backoff;
import org.springframework.retry.annotation.Retryable;
import org.springframework.stereotype.Component;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.RestTemplate;

import javax.inject.Inject;
import java.time.Duration;

import static java.lang.String.format;

/**
 * @author Tsigab Angosom Gebremedhin
 */

@Slf4j
@Component
public class OsloBysykkelConsumer {

    private static final int BACKOFF_DELAY = 500;
    private static final int BACKOFF_MULTIPLIER = 3;
    private static final long CONNECTION_TIMEOUT = 5000;
    private static final long READTIMEOUT = 15000;
    private static final String STATION_INFO = "station_information.json";
    private static final String STATION_STATUS = "station_status.json";

    private final RestTemplate restTemplate;
    private final OsloByykkelProp osloByykkelProp;

    @Inject
    public OsloBysykkelConsumer(RestTemplateBuilder restTemplateBuilder, OsloByykkelProp osloByykkelProp) {
        this.restTemplate = restTemplateBuilder
                .setConnectTimeout(Duration.ofMillis(CONNECTION_TIMEOUT))
                .setReadTimeout(Duration.ofMillis(READTIMEOUT))
                .build();
        this.osloByykkelProp = osloByykkelProp;
    }


    @Retryable(include = OsloBysykkelTechnicalException.class, backoff = @Backoff(delay = BACKOFF_DELAY, multiplier = BACKOFF_MULTIPLIER))
    public StationInfoResponse getSykkelStationInfo(final String orgName){
        HttpEntity entity = new HttpEntity<>(createHeaders(orgName));
        try {
            ResponseEntity<StationInfoResponse> response = restTemplate.exchange(osloByykkelProp.getUrl() + STATION_INFO, HttpMethod.GET,entity, StationInfoResponse.class);
            return response.getBody();
        } catch (HttpClientErrorException e) {
            log.warn("Kall mot Oslo bysykkel feilet med feilmelding = {}", e.getMessage());
            throw new OsloBysykkelFunctionalException(format("Kall mot Oslo bysykkel stationInfo feilet med statuscode=%s, feilmelding =%s",e.getStatusCode(), e.getMessage()));
        } catch (HttpServerErrorException e) {
            log.error("Kall mot Oslo bysykkel stationInfo feilet teknisk, feilmelding={}", e.getMessage() );
            throw new OsloBysykkelTechnicalException(format("Kall mot Oslo bysykkel stationInfo teknisk feilet med statuscode=%s, feilmelding =%s",e.getStatusCode(), e.getMessage()));
        }

    }

    @Retryable(include = OsloBysykkelTechnicalException.class, backoff = @Backoff(delay = BACKOFF_DELAY, multiplier = BACKOFF_MULTIPLIER))
    public StationStatusResponse getSykkelStationStatus(final String orgName){
        HttpEntity entity = new HttpEntity<>(createHeaders(orgName));
        try {
            ResponseEntity<StationStatusResponse> response = restTemplate.exchange(osloByykkelProp.getUrl() + STATION_STATUS, HttpMethod.GET,entity, StationStatusResponse.class);
            return response.getBody();
        } catch (HttpClientErrorException e) {
            log.error("Kall mot Oslo bysykkel feilet med feilmelding = {}", e.getMessage());
            throw new OsloBysykkelFunctionalException(format("Kall mot Oslo bysykkel status feilet med statuscode=%s, feilmelding =%s",e.getStatusCode(), e.getMessage()));
        } catch (HttpServerErrorException e) {
            log.error("Kall mot Oslo bysykkel satus feilet teknisk, feilmelding={}", e.getMessage() );
            throw new OsloBysykkelTechnicalException(format("Kall mot Oslo bysykkel status teknisk feilet med statuscode=%s, feilmelding =%s",e.getStatusCode(), e.getMessage()));
        }

    }


    private HttpHeaders createHeaders(String orgName) {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.add("Client-Identifier", orgName  + "-" + osloByykkelProp.getAppName());
        return headers;
    }


}
