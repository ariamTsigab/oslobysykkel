package org.oslo.oslobysykkel.exception;

public class OsloBysykkelFunctionalException extends RuntimeException {

    public OsloBysykkelFunctionalException(String message) {
        super(message);
    }

    public OsloBysykkelFunctionalException(String message, Throwable cause) {
        super(message, cause);
    }
}
