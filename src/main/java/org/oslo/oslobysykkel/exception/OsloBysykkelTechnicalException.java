package org.oslo.oslobysykkel.exception;

public class OsloBysykkelTechnicalException extends RuntimeException {

    public OsloBysykkelTechnicalException(String message) {
        super(message);
    }

    public OsloBysykkelTechnicalException(String message, Throwable cause) {
        super(message, cause);
    }
}
