package org.oslo.oslobysykkel.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class StationStatusResponse {

    private Integer last_updated;
    private StationStatusData data;

    @Data
    @Builder
    @AllArgsConstructor
    @NoArgsConstructor
    public static class StationStatusData {
        private List<Station> stations;
    }

    @Data
    @Builder
    public static class Station {
        private String station_id;
        private Integer num_bikes_available;
        private Integer num_docks_available;
    }
}
