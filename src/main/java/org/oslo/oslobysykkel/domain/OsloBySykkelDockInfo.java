package org.oslo.oslobysykkel.domain;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class OsloBySykkelDockInfo {

    private String stationId;
    private String stationName;
    private String address;
    private Integer availableBike;
    private Integer availableDocks;
}
