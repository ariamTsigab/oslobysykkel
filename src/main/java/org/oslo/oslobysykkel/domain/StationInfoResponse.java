package org.oslo.oslobysykkel.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class StationInfoResponse {

    private Integer last_updated;
    private StationData data;

    @Data
    @Builder
    @AllArgsConstructor
    @NoArgsConstructor
    public static class StationData {
        private List<Station> stations;
    }

    @Data
    @Builder
    public static class Station {
        private String station_id;
        private String name;
        private String address;
        private String capacity;
    }
}
