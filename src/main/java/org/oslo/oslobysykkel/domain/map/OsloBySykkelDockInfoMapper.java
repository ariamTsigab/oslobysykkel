package org.oslo.oslobysykkel.domain.map;

import org.oslo.oslobysykkel.domain.OsloBySykkelDockInfo;
import org.oslo.oslobysykkel.domain.StationInfoResponse;
import org.oslo.oslobysykkel.domain.StationStatusResponse;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

public class OsloBySykkelDockInfoMapper {

    public List<OsloBySykkelDockInfo> map(StationInfoResponse stationInfoResponse,
                                          StationStatusResponse stationStatusResponse) {

        if(isBysykkelInfoNull(stationInfoResponse)) {
            return Collections.emptyList();
        } else {
            return stationInfoResponse.getData().getStations().stream()
                    .filter(Objects::nonNull)
                    .map(stationInfo -> {
                        if(isBysykkelStatusNull(stationStatusResponse)) {
                            List<OsloBySykkelDockInfo> osloBysykkelStations = new ArrayList<>();
                            osloBysykkelStations.add(OsloBySykkelDockInfo.builder()
                                    .stationId(stationInfo.getStation_id())
                                    .stationName(stationInfo.getName())
                                    .address(stationInfo.getAddress()).build());
                            return osloBysykkelStations;
                        } else {
                            return stationStatusResponse.getData().getStations().stream()
                                    .filter(status -> status.getStation_id().equals(stationInfo.getStation_id()))
                                    .map(status -> OsloBySykkelDockInfo.builder()
                                            .stationId(stationInfo.getStation_id())
                                            .stationName(stationInfo.getName())
                                            .address(stationInfo.getAddress())
                                            .availableBike(status.getNum_bikes_available())
                                            .availableDocks(status.getNum_docks_available()).build()
                                    ).collect(Collectors.toList());
                        }
                    }).flatMap(Collection::stream).filter(Objects::nonNull).collect(Collectors.toList());

        }

    }


    private boolean isBysykkelStatusNull(StationStatusResponse stationStatusResponse) {
        return  (stationStatusResponse==null || stationStatusResponse.getData() ==null);
    }

    private boolean isBysykkelInfoNull(StationInfoResponse stationInfoResponse) {
        return  (stationInfoResponse==null || stationInfoResponse.getData() ==null);
    }

}
