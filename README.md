# OSLO BYSYKKEL

Tjenesten henter en liste over tilgjengelige låser og ledige sykler fra de ulike Oslo Bysykkel stasjoner. 
 
- Teknologi: Java 11, SpringBoot og Maven

## Bygge

```sh
$ mvn clean install
```

## Kjøre og Starte

```sh
$ mvn spring-boot:run
```

## Åpne
 I nettleseren eller postman med følgene link
 
```diff
 http://{hostname}/rest/api/{firma/organisasjon}/stations
```

 og i terminal 

```diff
curl -X GET http://{hostname}/rest/api/{firma/organisasjon}/stations
```

### NB
- Husk å sette hostname og firma/organisasjon i [url path]
- for eksempel http://localhost:8080/rest/api/hosariam/stations


### Henvendelser
Spørsmål koden eller prosjekttet kan rettes til [Tsigab Angosom](tsigab.angosom@gmail.com).